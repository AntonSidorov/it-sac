/**
 * @fileoverview priceFormat text pipe
 * @author Anton Sidorov
 * @copyright (c) 2017 Anton Sidorov
 * @license MIT
 * @version 0.0.1
 * @description This is an angular pipe implementation of the formatNumber function. Used in html like this: '{{value | priceFormat}}', will return the same number but always with 2 decimal places.
 */
import { Pipe, PipeTransform } from '@angular/core';
import { formatNumber } from './item';

@Pipe({
  name: 'priceFormat'
})
export class PriceFormatPipe implements PipeTransform {

  transform(value: number, args?: any): any {
    return formatNumber(value);
  }
}