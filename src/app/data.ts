/**
* @fileoverview Supplied data for 'Luppino Fresh' Dashboard
* @author Anton Sidorov
* @copyright (c) 2017 Anton Sidorov
* @license MIT
* @version 0.0.1
* @description All the data supplied in the project brief is located here.
*/

import { Category, Item } from './item';


export var CATEGORIES: Category[] = [
    {
        id: 0,
        name: "Fruit",
    },
    {
        id: 1,
        name: "Vegetables"
    }
];

export var ITEMS: Item[] = [
    //0: Fruit, 1: Vegetables
    new Item('Fuji Apples', 5.00, CATEGORIES[0]),
    new Item('Jonathan Apples', 4.50, CATEGORIES[0], .25),
    new Item('Carrots', 1.50, CATEGORIES[1]),
    new Item('Bananas', 2.50, CATEGORIES[0], .25),
    new Item('Long Beans', 7.00, CATEGORIES[1]),
    new Item('Brushed Potatoes', 5.00, CATEGORIES[1]),
    new Item('Nectarines (Yellow)', 3.50, CATEGORIES[0], .25),
    new Item('Washed Potatoes', 6.00, CATEGORIES[1]),
    new Item('Butter Beans', 7.00, CATEGORIES[1]),
    new Item('Strawberries', 4.00, CATEGORIES[0], .25),
    new Item('Nectarines (White)', 3.50, CATEGORIES[0]),
];

