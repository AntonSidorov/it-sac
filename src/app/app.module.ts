/**
* @fileoverview Angular Module file for the 'Luppino Fresh' 
* @author Anton Sidorov
* @copyright (c) 2017 Anton Sidorov
* @license MIT
* @version 0.0.1
* @description This is just a basic angular module for this application to function properly.
*/

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { AntUiModule } from 'antsui';
import { PriceFormatPipe } from './price-format.pipe';

@NgModule({
  declarations: [
    AppComponent,
    PriceFormatPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    AntUiModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
