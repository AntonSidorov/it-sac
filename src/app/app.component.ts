/**
 * @fileoverview Root component of the 'Lupino Fresh' Angular app. Contains the logic for the data manipulation,etc.
 * @author Anton Sidorov
 * @copyright (c) 2017 Anton Sidorov
 * @license MIT
 * @version 0.0.1
 * @description This component is just a simple angular component, here's an explanation of most of the logic. 
 * VARIABLES (and lambda functions)
 * search - the variable bound to the search textbox. Updates automatically due to angular.
 * searchTerms - a function which returns the search terms (an array of words in the search box)
 * items - an array of ALL items in the 'database', updated when items are added/deleted
 * categories -  an array of ALL categories in the 'database', updated when categories are added/deleted
 * newCategory - variable bound to the Category textbox, used for creating a new category. 
 * newItem - the currently edited new Item. Usually contains a basic item with default values, unless edited by user.
 * saleItems - a function that returns a sorted (by alphabet) array of items that have any kind of discount. 
 *    Uses a filter function rather than the pseudocode from the design as it is more compact and is fairly easy to understand (with understanding of lambdas)
 * sorting - 
 * sortName(Desc), sortCategory(Desc) sortPrice(Desc) - Ascending and descending comparer functions for the sort algorithm. Used for respective column sorting 
 *    These functions return boolean values on whether or not an item should be below or above another item in the sorted array. The function has to return true if an item is below, false if above.
 *    sortName(Desc) simply compares the strings, as they are assumed to be unique in the project requirements
 *    sortCategory(Desc) compares the category and then if the category is the same compares the name in the same order by calling the sortName(Desc) functions.
 *    sortPrice(Desc) compares the display price and then if the price is the same compares the name in the same order by calling the sortName(Desc) functions. 
 * filteredItems - a function that returns an array of values that match any of the search parameters, by applying boolean OR to 5 conditions, where each of the conditions checks if the property contains at least one of the search terms
 * headers - contains the header information for the table headers. Their display is generated from this. The sort functions are also stored here for easier access during sort press.
 * 
 * FUNCTIONS
 * swap() - a simple array variable swap function
 * selectsort() - an implementation of a select sort, which accepts a comparer function.
 * sortItems() - a function that sorts items based on the sort selected. Calls sort() for the logic. 
 * sort() - a function which changes the sort value of a certain header, resetting other headers.
 * deleteItem() - removes an item from the array, calls the sort function again.
 * addCategory() - creates a new category
 * addItem() - creates a new item from the values user inputted. (Validation is kind of present, but may not be visible to the user)
 * 
 */
import { Component, HostBinding, OnInit } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Item, Category } from './item';
import { ITEMS, CATEGORIES } from './data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  search = '';
  searchTerms = () => this.search.split(' ').filter(v => v != '');
  items: Item[];
  categories = () => CATEGORIES;

  newCategory: string = '';
  newItem: Item = new Item('', 0, CATEGORIES[0]);

  saleItems = () => this.selectsort(this.items.filter(item => item.discount > 0), this.sortName);

  sortName = (a, b) => a.name < b.name;
  sortNameDesc = (a, b) => a.name > b.name;

  sortCategory = (a, b) => a.category.name < b.category.name ? true : a.category.name == b.category.name ? this.sortName(a, b) : false;
  sortCategoryDesc = (a, b) => a.category.name > b.category.name ? true : a.category.name == b.category.name ? this.sortNameDesc(a, b) : false;

  sortPrice = (a, b) => a.discountPrice() < b.discountPrice() ? true : a.category.name == b.category.name ? this.sortName(a, b) : false;
  sortPriceDesc = (a, b) => a.discountPrice() > b.discountPrice() ? true : a.category.name == b.category.name ? this.sortNameDesc(a, b) : false;

  filteredItems = () => this.searchTerms().length == 0 ? this.items :
    this.items.filter(i => {
      return this.searchTerms().filter(s => i.category.name.toLowerCase().indexOf(s.toLowerCase()) > -1).length > 0 ||
        this.searchTerms().filter(s => i.name.toLowerCase().indexOf(s.toLowerCase()) > -1).length > 0 ||
        this.searchTerms().filter(s => i.displayPrice().toLowerCase().indexOf(s.toLowerCase()) > -1).length > 0 ||
        this.searchTerms().filter(s => i.savingsFormatted().toLowerCase().indexOf(s.toLowerCase()) > -1).length > 0 ||
        this.searchTerms().filter(s => i.originalPrice.toPrecision(3).toLowerCase().indexOf(s.toLowerCase()) > -1).length > 0;
    });

  //-1 - no sorting, 0 - sorting ascending, 1 - sorting descending
  headers = [
    { title: 'Name', sort: 0, sortFuncs: [this.sortName, this.sortNameDesc] },
    { title: 'Category', sort: -1, sortFuncs: [this.sortCategory, this.sortCategoryDesc] },
    { title: 'Price ($/kg)', sort: -1, sortFuncs: [this.sortPrice, this.sortPriceDesc] }
  ]
  constructor() {
    this.items = ITEMS;
  }
  ngOnInit() {
    this.sortItems();
  }

  sort(header) {
    header.sort += 1;
    if (this.items.length == 0) return;
    if (header.sort == 2) header.sort = -1;
    //To refrain people from unsorting.
    if (this.headers.filter(h => h.sort != -1).length == 0) header.sort = 0;
    var index = this.headers.indexOf(header);
    //reset sorting for other headers
    this.headers.filter((v, i) => i != index).map(h => h.sort = -1);

    //call the sort array function name(params:type) {
    this.sortItems(header);
  }

  sortItems(header = this.headers.filter(h => h.sort != -1)[0]) {
    if (this.items.length == 0) return;
    this.items = this.selectsort(this.items, header.sortFuncs[header.sort]);
  }

  swap = (arr, i, j) => { var k = arr[i]; arr[i] = arr[j]; arr[j] = k }

  selectsort(arr, comparer = (a, b) => a < b) {
    if (arr.lenfth == 0) return [];
    if (arr.length == 1) return arr;
    for (var i = 0; i < arr.length; i++)
      if (comparer(arr[i], arr[0]))
        this.swap(arr, i, 0);
    return [arr[0]].concat(this.selectsort(arr.slice(1, arr.length), comparer));
  }

  deleteItem(item: Item) {
    this.items = this.items.filter(v => v != item);
    this.sortItems();
  }

  addCategory() {
    if (this.newCategory == '') return;
    if (CATEGORIES.map(v => v.name.toLowerCase()).every(v => v != this.newCategory.toLowerCase()))
      CATEGORIES[CATEGORIES.length] = { id: CATEGORIES.length, name: this.newCategory };
    this.newCategory = '';
  }

  addItem() {
    this.newItem.discount /= 100;
    this.items.push(this.newItem);
    this.sortItems();
    this.newItem = new Item('', 0, CATEGORIES[0]);
  }

}

export class Filter {
  propery: string;
  value: string;
}