/**
 * @fileoverview Data structures for 'Luppino Fresh' Dashboard
 * @author Anton Sidorov
 * @copyright (c) 2017 Anton Sidorov
 * @license MIT
 * @version 0.0.1
 * @description The two data structures used in this project are located here. 
 *
 * Category - A simple class with an id to identify it (which can be used in the future, for storage) and a name that the user will see.
 * Item - Represents an item that 'Luppino Fresh' sells, has a name, a reference to the category that it's part of and the price/discount.
 * The originalPrice is the price from which all calculation are made and it is the price that the user can input
 * discount is a number between 0 and 1 which represents the discount percentage.
 * 
 * For the rest of the properties, functions are used to compute the value, which are then called upon by Angular when it detects a change related to one of those values.
 * All of the calculations that return a price have rounding to nearest cent (the prices are rounded up and the )
 * 
 * discountPrice is a function that returns the discount multiplied by the original price. If there isn't a discount, the discountPrice is just the 
 * savings is a function that subtracts the the discountPrice from the originalPrice
 * savingsFormatted is a formatted string value of savings with 2 decimal places. 
 * displayPrice is a formatted string value of the discount price.
 * 
 * The formatNumber function is a small function that shows a number to 2 decimal places 
 */

export class Category {
    id: number;
    name: string;
}

export class Item {
    name: string;
    category: Category;
    originalPrice: number;
    savings = () => Math.floor((this.originalPrice - this.discountPrice()) * 100) / 100;
    savingsFormatted = () => this.savings() == 0 ? '' : formatNumber(this.savings());
    discountPrice = () => this.originalPrice * (1 - this.discount);
    discount: number = 0;
    displayPrice = () => formatNumber(Math.ceil(this.discountPrice() * 100) / 100);
    constructor(name: string, price: number, category: Category, discount: number = 0) {
        this.name = name;
        this.originalPrice = price;
        this.category = category;
        this.discount = discount;
    }
}

export function formatNumber(val: number): string {
    var log10 = Math.floor(Math.log10(val));
    if (log10 < 0) log10 = 0;
    return val.toPrecision(log10 + 3).slice(0, log10 + 4);
}