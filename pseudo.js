// SEARCH PSEUDOCODE v1
// This module assumed that the management of the search term, items, and calling of the search function itself is called by another (parent) program module.
// It also assumes that the display of an object is handled by that parent module, which is why the result of the search is an array of objects, not actual display of objects.
// 
// Outline of the procedure: When the search function is called, the algorithm goes through each item in the array, checking if a certain property of the current item satisfies the criteria. If so, it adds that property to a new array.
// searchTerm, searchProperty can be replaced (and probabaly will be) with a hashtable/dictionary and can follow a boolean operator specified somewhere else
// items are provided by the parent module and searchedItems are consumed by the parent module. In the actual program, the values may be returned by the function instead of being saved to an array.
// 

//The search term that is changed through the user interface
//In reality, this can be an array/list of properties and their respective terms (plus a variable for the boolean operator)
searchTerm = '', searchProperty = '';

//An array of the items that will be searched through, 
items = [];
//An array that will contain the sorted items when they're found
searchedItems = [];

function search() {
  //Clear the array, to make sure the search works
  searchedItems = [];
  //A basic repetition for each of the item in items
  //In reality, this is likely to be replaced by array.filter which does exactly the same, except in much shorter form
  for (i = 0; i < items.length; i++)
    //item[searchProperty] gets the property value for current item
    //.contains is a generic function that tests whether or not a string contains another string. In reality, this could be a special format string and can be implemented in different languages differently (in js, it is .indexOf)
    if (item[searchProperty].indexOf(searchTerm))
      searchedItems.push(item);
}

//