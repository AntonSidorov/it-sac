import { ItDeenPage } from './app.po';

describe('it-deen App', function() {
  let page: ItDeenPage;

  beforeEach(() => {
    page = new ItDeenPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
